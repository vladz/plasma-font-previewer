/*
    SPDX-FileCopyrightText: 2022 Vlad Zahorodnii <vlad.zahorodnii@kde.org>

    SPDX-License-Identifier: GPL-3.0-or-later
*/

import QtQuick 2.15

import org.kde.kirigami 2.10 as Kirigami

Kirigami.Page {
    title: "Font"

    Rectangle {
        width: 800
        height: 600
        color: "green"
    }
}