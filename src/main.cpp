/*
    SPDX-FileCopyrightText: 2022 Vlad Zahorodnii <vlad.zahorodnii@kde.org>

    SPDX-License-Identifier: GPL-3.0-or-later
*/

#include <QGuiApplication>
#include <QCommandLineParser>
#include <QDebug>
#include <QQmlApplicationEngine>
#include <QUrl>

#include <KAboutData>
#include <KLocalizedString>
#include <KQuickAddons/QtQuickSettings>

int main(int argc, char **argv)
{
    QGuiApplication app(argc, argv);

    KAboutData aboutData(QStringLiteral("plasma-font-previewer"),
                         i18n("Font Previewer"),
                         QStringLiteral(PROJECT_VERSION),
                         i18n("An application for previewing and installing fonts"),
                         KAboutLicense::GPL_V3);
    aboutData.setDesktopFileName(QStringLiteral("org.kde.plasma-font-previewer"));

    aboutData.addAuthor(i18n("Vlad Zahorodnii"), QStringLiteral("Development"), QStringLiteral("vlad.zahorodnii@kde.org"));
    KAboutData::setApplicationData(aboutData);

    QCommandLineParser parser;
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument(QStringLiteral("font"), i18n("File path to the font"));
    parser.process(app);

    if (parser.positionalArguments().count() != 1) {
        parser.showHelp(-1);
    }

    KQuickAddons::QtQuickSettings::init();

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));

    return app.exec();
}
